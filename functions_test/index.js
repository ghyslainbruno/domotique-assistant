'use strict';

process.env.DEBUG = 'actions-on-google:*';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const request = require('request');
const { DialogflowApp } = require('actions-on-google');
const functions = require('firebase-functions');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.post('/domotique', function(request, response) {
    const app = new DialogflowApp({request, response});
    console.log('Request headers: ' + JSON.stringify(request.headers));
    console.log('Request body: ' + JSON.stringify(request.body));

    // Fulfill action business logic
    function lightsHandler (app) {
        // Complete your fulfillment logic and send a response
        const request = require('request');

        const command = app.body_.result.parameters.lights;

        switch(command) {
            case 'on':
                request.post('http://domotique.ghyslain.xyz/all_on', {form:{key:'value'}});
                break;
            case 'off':
                request.post('http://domotique.ghyslain.xyz/all_off', {form:{key:'value'}});
                break;

        }

        app.ask('Ok !');
    }

    function modesHandler (app) {
        const request = require('request');

        const mode = app.body_.result.parameters;

        const modeAsked = Object.keys(app.body_.result.parameters)[0];

        switch(modeAsked) {
            case 'night':
                request.get('http://domotique.ghyslain.xyz/night');
                app.tell('Bonne nuit !');
                break;
            case 'morning':
                request.get('http://domotique.ghyslain.xyz/morning');
                app.tell('Bonjour !');
                break;
            case 'evening':
                request.get('http://domotique.ghyslain.xyz/evening');
                app.tell('Bonne soirée !');
                break;
        }
    }

    function shuttersHandler (app) {

        const request = require('request');
        const shutters = app.body_.result.parameters;
        const shuttersActionAsked = app.body_.result.parameters.shutters;

        switch(shuttersActionAsked) {
            case 'up':
                request.post('http://domotique.ghyslain.xyz/up');
                app.tell('Ok');
                break;
            case 'pause':
                request.post('http://domotique.ghyslain.xyz/pause');
                app.tell('Ok');
                break;
            case 'close':
                request.post('http://domotique.ghyslain.xyz/down');
                app.tell('Ok');
                break;
        }
    }

    const actionMap = new Map();
    actionMap.set('lights.actions', lightsHandler);
    actionMap.set('modes', modesHandler);
    actionMap.set('shutters.actions', shuttersHandler);

    app.handleRequest(actionMap);
});

const server = app.listen(81, function () {
    console.log('Server starting - listening on port ' + 81)
});